library(openxlsx) # import xlsx
library(jsonlite)
library(tidyverse)
library(readxl)

#### SetUp Structure, removing content analysis from original data ####
dat_orig <- read_excel('data/Coding Example.xlsx')
dat <- tibble(
  id = dat_orig$id[complete.cases(dat_orig$elaboration)],
  wish = dat_orig$wish[complete.cases(dat_orig$elaboration)],
  elaboration = dat_orig$elaboration[complete.cases(dat_orig$elaboration)],
  api_response = NA,
  strategies = list(NA),
  used_strat = NA
  
)


#### import existing structure, if using existing data file ####
#dat <- fromJSON('data/contrast_gpt35.json')
#dat <- fromJSON('data/contrast_gpt4.json') ## data from promt 2.1 with chat gpt-3.5 (bit more persuasive with classification output at lower temperature)



### GPT loop libraries###
library(tm)
library(reticulate)

# importing the OpenAI API key from file (and specific line)
Sys.setenv(OPENAI_API_KEY=read.csv("api_key.csv", header = FALSE)[1, 1])

# for the API requests, python and the python library 'openai' is needed to be installed beforehand.
openai <- import("openai")
client <- openai$OpenAI()





### storing differnet promts for test trial

#         "content" = "You will get a statement which you have to seperate and then classify.
#         Seperate the input so that each piece can be attributed to either 'desired future', 'present reality' or 'other'. Output to a format were you copy the piece of the input and then seperated with a bar (|) the attributes, then ending with double semicolon;;. Start with the next piece of text until no text is left.
#         Here is an example of a possible input: 'I need to get into better shape to be more attractive. I need to be more social and meet more people. Both of these combined should help me to achieve my main goal.' 
#         and your output format: 'I need to get into better shape to be more attractive|present reality;; I need to be more social|present reality;; and meet more people|present reality;; Both of these combined should help me to achieve my main goal.|desired future;;'
#         Dont add anything to the text pieces, just use the input and the classification words"),


#    "content" = "Here is an example of a possible input: 'I have been together with my boyfriend for over 11 years now.  We have been living together for just over 10 of those years.  People who didnt personally know us would probably assume we are married but we are not.  For some reason, he has not popped the question yet.  I think he may be afraid because his first marriage did not work. At first it didnt bother me but now I am getting to the point where I really WANT to get married.  I want to make it official.  I want him to be my husband and I want to be his wife.  I want to be an official stepmom to his kids.  I think I have waited long enough and really deserve this.'
#    And your output format: 'I have been together with my boyfriend for over 11 years now|other;; We have been living together for just over 10 of those years|other;; People who didnt personally know us |other;; probably assume we are married|other;; but we are not|other;; For some reason|other;; he has not popped the question yet|other;; I think he may be afraid|other;; because his first marriage did not work|present reality;; At first it didnt bother me|present reality;; but now I am getting to the point|other;; where I really WANT to get married|other;; I want to make it official|desired future;; I want him to be my husband|desired future;; and I want to be his wife|desired future;; I want to be an official stepmom to his kids|desired future;; I think I have waited long enough|other;; and really deserve this|other;;'
#    Dont add anything to the text pieces, just use the input and the classification words I gave you! Make sure to not make anything up, just use 'desired future', 'present reality' or 'other', nothing else as classification code!"),


### The content of the system role can be understood as an instruction to the model. Like an instruction of what is supposed to happen with the input form the user role.


system <- "You will get a text which you have to seperate and then classify.

A statement is defined as a phrase consisting of no more than one subject–predicate–object–adverb sequence.
For example, if a student wrote 'I dream about finding a spontaneous and affectionate partner, who is also down to earth,' this text is segmented into three statements: 
I dream about finding a spontaneous 
and affectionate partner, 
who is also down to earth.

Each seperated statement is supposed to be coded into either 'desired future', 'present reality' or 'other'. Each coding is defined as follows:
desired future:
Descriptions of the desired future (“I would like to get to know some people with whom I can spend my leisure time”).
Consequences of attaining the desired future
 feelings (“starting a family would make me very happy”) 
 events (“our whole family would be together at Christmas”)
 material gains (“moving in together allows us to afford a bigger apartment”)
 nonmaterial gains (“I would have someone to talk to”)
 improvements of current situation (“I would not feel lonely anymore”)

present reality
Descriptions of the present reality (“currently I am not very satisfied”)
Obstacles in the present reality to attaining the desired future
 internal (“I am still mad at my brother”)
 external (“it is difficult to find a nice apartment”)
 potential (“I might get sick')

other
Statements that could not be categorized as pertaining to the desired future or to the present reality:
Ambiguous ('I have to take risks')
Past ('we always had a lot of fun together')
Self in general ('I am studying Psychology')
Experimental situation ('I hope I win the lottery')

Really make sure to stick to these definitions and to not miss ambiguous descriptions as 'other'!

Output to a format were you copy the piece of the input and then seperated with a bar (|) the attributes, then ending with double semicolon;;. Start with the next piece of text until no text is left. This is how an example:
Here is an example of a possible input: 'I have been together with my boyfriend for over 11 years now.  We have been living together for just over 10 of those years.  People who didnt personally know us would probably assume we are married but we are not.  For some reason, he has not popped the question yet.  I think he may be afraid because his first marriage did not work. At first it didnt bother me but now I am getting to the point where I really WANT to get married.  I want to make it official.  I want him to be my husband and I want to be his wife.  I want to be an official stepmom to his kids.  I think I have waited long enough and really deserve this.'
And your output format: 'I have been together with my boyfriend for over 11 years now|other;; We have been living together for just over 10 of those years|other;; People who didnt personally know us |other;; probably assume we are married|other;; but we are not|other;; For some reason|other;; he has not popped the question yet|other;; I think he may be afraid|other;; because his first marriage did not work|present reality;; At first it didnt bother me|present realitty;; but now I am getting to the point|other;; where I really WANT to get married|other;; I want to make it official|desired future;; I want him to be my husband|desired future;; and I want to be his wife|desired future;; I want to be an official stepmom to his kids|desired future;; I think I have waited long enough|other;; and really deserve this|other;;'

Dont add anything to the text pieces, just use the input and the classification words I gave you! Make sure to not make anything up, just use 'desired future', 'present reality' or 'other', nothing else as classification code!"


chat_gpt <- function(prompt) {
  message <- list(
    list("role" = "system",
         "content" = system),
    
    list("role" = "user",
         "content" = prompt)
  )
  response <- openai$chat$completions$create(
    model="gpt-3.5-turbo",
    messages=message,
    temperature=0.1
  )
  
  #return(response[['choices']][[1]][['message']][['content']])
  return(gsub("\n", "", response[['choices']][[1]][['message']][['content']])) # returning the actual response of the api-return
}


### looping through elaborations, applying the chat_gpt function to each elaboration, if there is no api_response yet.
for (i in 1:nrow(dat)){
  if(is.na(dat$api_response[i])){
    query <- paste(dat$elaboration[i])
    dat$api_response[i] <- chat_gpt(query)
    # print update
    print(paste0(i,"/",nrow(dat), " | ",dat$api_response[i], " | ", sep = ""))
  }
  else print('obsolete')
  rm(query, i)
}

# using apply to form responses into df. Transforming the response (bare list of strings) into columns.
dat$strategies <- apply(dat, 1, function(row) {
  rows <- strsplit(row[['api_response']], ";;")[[1]]
  rows_split <- strsplit(rows, "\\|")
  df <- tibble(
    statement = sapply(rows_split, `[`, 1),
    coding = sapply(rows_split, `[`, 2)
  )
  return(df)
})




## cleaning responses from chatGPT hallucinations (not really needed at low GPT temperature)
dat  |>
  mutate(coding = map(strategies ~ case_when(
    statement == "desired reality" ~ "desired reality",
    statement == "desired future" ~ "desired future",
    statement == "desired characteristic" ~ "desired reality",
    statement == "past reality" ~ "present reality",
    TRUE ~ coding  # Assuming you meant to retain original coding if no condition matches
  )))



## function for coding the strategy columns
get_strategy <- function(strategies) {
  if ("desired future" %in% strategies && "present reality" %in% strategies) {
    if (which(strategies == "desired future")[1] < which(strategies == "present reality")[1]) {
      return("MC")
    } else {
      return("RMC")
    }
  } else if ("desired future" %in% strategies) {
    return("Fantasising")
  } else if ("present reality" %in% strategies) {
    return("Indulging")
  } else {
    return("Other")
  }
}

# applying the coding function from above
dat <- dat  |>
  mutate(used_strat = map(strategies, ~ get_strategy(.x$coding)))  |>
  unnest(cols = c(used_strat))





# nested data can be easily saved into JSON files
jsonlite::write_json(dat, "data/contrast_gpt35_5.json")