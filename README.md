# Metal Contrasting - Content Analysis - OpenAI adaptation

This is the initial commit with the r-script. Annotation and minimal documentation are in the script itself. 


### Input Data
The original data file used, has a structure as the following:

id|wish|elaboration|no.statements|statements.number|statements|coding|desired.future|present.reality|other|strategy
-|-|-|-|-|-|-|-|-|-|-

### Output data
The output data is shrunken down to a nested structure. With the strategies column with each statement and coding beside each elaboration.
id|wish|elaboration|api_response|strategies|used_strat
-|-|-|-|-|-
